//
//  GemeApp.swift
//  Geme
//
//  Created by Karl Rink on 10/7/23.
//

import SwiftUI

@main
struct GemeApp: App {
    var body: some Scene {
        WindowGroup {
            ConversationScreen()
        }
    }
}
